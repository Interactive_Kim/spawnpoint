let mix = require('laravel-mix');

mix.sass('dev/src/scss/styles.scss', 'public/css/app.min.css')
    .options({
        processCssUrls: false // used for base urls
    })
    .scripts(
        [
            // 'node_modules/jquery/dist/jquery.min.js',
            'node_modules/bootstrap/dist/js/bootstrap.bundle.js',

            'dev/src/js/browser.js',
            // 'dev/src/js/app.js',
        ],
        // render compiled js to dev folder an copy with task to files
        'public/js/app.min.js',
        './',
    );

//
// copy
//

// copy images
mix.copy('dev/src/img', 'public/img');
console.log('###Images copied ###');

// copy svg
mix.copy('dev/src/svg', 'public/svg');
console.log('###Images copied ###');

//
// owl carousel
//

const webpack = require('webpack');


plugins: [
    new webpack.ProvidePlugin({
        $: 'jquery',
        jQuery: 'jquery',
        'window.jQuery': 'jquery'
    }),
],
